BUILDDIR := build
EMULATOR := bgb  # sadly proprietary
NAME := boilerplate

all: $(BUILDDIR)/$(NAME).gb

$(BUILDDIR)/$(NAME).gb: $(BUILDDIR)/main.o
	rgblink -m $(BUILDDIR)/$(NAME).map -n $(BUILDDIR)/$(NAME).sym -o $(BUILDDIR)/$(NAME).gb $(BUILDDIR)/main.o
	rgbfix -v -p 0 $(BUILDDIR)/$(NAME).gb  # pad the rom, add the logo etc.

$(BUILDDIR)/main.o: src/main.asm $(BUILDDIR)/tiles.bin
	rgbasm -i src/ -i $(BUILDDIR)/ -o $(BUILDDIR)/main.o src/main.asm

$(BUILDDIR)/tiles.bin: images/tiles.png
	mkdir -p $(BUILDDIR)
	rgbgfx -o $(BUILDDIR)/tiles.bin images/tiles.png

run: $(BUILDDIR)/$(NAME).gb
	$(EMULATOR) $(BUILDDIR)/$(NAME).gb

# delete the last build, recompile and run in the emulator
dev: clean run

clean:
	rm -r $(BUILDDIR)
