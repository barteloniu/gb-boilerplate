GameLoop:
	halt  ; stop the system clock
	nop  ; prevent the bug

	; check if a VBlank interrupt has happened
	ld a, [VBlankFlag]
	and a
	jr z, GameLoop  ; if not, go keep on waiting

	; reset the flag
	xor a
	ld [VBlankFlag], a

	jr GameLoop
