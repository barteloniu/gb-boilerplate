INCLUDE "hardware.inc"
INCLUDE "constants.asm"


SECTION "Header", ROM0[$100]
EntryPoint:
	nop
	jp Init

; space for the logo
REPT $150-$104
	db 0
ENDR


SECTION "Variables", WRAM0
INCLUDE "variables.asm"


SECTION "VBlank", ROM0[$40]
INCLUDE "vblank.asm"


SECTION "Game", ROM0
INCLUDE "init.asm"

; executed immediately
INCLUDE "gameloop.asm"


; called from somewhere
INCLUDE "memory.asm"
INCLUDE "initvariables.asm"
INCLUDE "graphics.asm"
INCLUDE "input.asm"


SECTION "Tiles", ROM0
Tiles:
INCBIN "tiles.bin"
TilesEnd:


SECTION "Strings", ROM0
INCLUDE "charmap.asm"
INCLUDE "strings.asm"
