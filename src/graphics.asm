WaitForVBlank:
	; check if the screen is turned off
	ld a, [rLCDC]
	and a
	jr z, .return  ; if so don't check LY

.loop
	ld a, [rLY]
	cp 144 ; the screen is 144 pixels tall
	jr c, .loop

.return
	ret

UpdateGraphics:
	; let the rest know that a VBlank interrupt has been triggered
	ld a, 1
	ld [VBlankFlag], a

	call GetInput

	; get a black square when a button is pressed
	GetTilePos BG_START, 1, 2  ; start, x, y
	ld a, [ButtonsPressed]
	and a
	jr z, .disappear

.appear
	ld a, 1
	jr .settile

.disappear
	xor a  ; a = 0

.settile
	ld [POS], a

.return
	ret
