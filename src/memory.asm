MemCopy:
	; SOURCE: de
	; DEST: hl
	; COUNTER: bc

.loop
	ld a, [de]  ; get a byte from the source
	ld [hl+], a  ; place it in the destination and increment hl
	inc de  ; move to the next byte
	dec bc  ; decrement the counter
	
	; check if we finished
	; this weird 16bit method must be used since "dec bc" doesn't update flags
	ld a, b
	or c
	jr nz, .loop

	ret


MemSet:
	; SOURCE: d
	; DEST: hl
	; COUNTER: bc

.loop
	ld a, d  ; grab the source
	ld [hl+], a  ; set the byte and move on to the nex one
	dec bc  ; decrement the counter

	; check if we're done
	ld a, b
	or c
	jr nz, .loop

	ret


StringCopy:
	; SOURCE: de
	; DEST: hl

.loop
	ld a, [de]  ; get a character from the source
	ld [hl+], a  ; put it in the destination and move to the next tile
	inc de  ; move on to the next char
	and a  ; check if we got to a null byte
	jr nz, .loop  ; if we didn't carry on

	ret


StringPrint:
	; SOURCE: de
	; DEST: hl

	; prints text but omits the nullbyte and understands newlines

	push hl  ; save the initial position

.loop
	ld a, [de]  ; get the first character from the source

	; check if it's a nullbyte
	and a
	jr z, .return  ; if so quit

	; check for a newline
	cp NEWLINE
	jr nz, .copy  ; if it is not '\n' carry on

	; go to the next line
	pop hl  ; read the beginning of the line
	ld bc, $20  ; the background and window are $20 tiles wide
	add hl, bc  ; move to the next line
	push hl  ; save the new position
	inc de  ; move to the next char
	jr .loop  ; don't print '\n'
	
.copy
	ld [hl+], a  ; save at the destination and move to the nex ttile
	inc de  ; next char
	jr .loop

.return
	pop hl  ; get rid of the saved position
	ret
