Init:
	di  ; disable interrupts
	ld sp, $fffe  ; set the stack pointer to the top of the high ram

	call InitVariables

	call WaitForVBlank

	; turn the LCD off
	xor a  ; a = 0
	ld [rLCDC], a

	; copy the tiles
	ld hl, TILE_DATA_START  ; the beginning of the tile data table
	ld de, Tiles  ; source
	ld bc, TilesEnd - Tiles  ; counter
	call MemCopy
	
	; clean the background tiles
	ld hl, BG_START  ; top-left corner
	ld d, 0  ; data - clean screen
	ld bc, 32 * 32  ; the whole thing
	call MemSet

	; print the welcome message
	GetTilePos BG_START, 1, 1  ; start, x, y
	ld hl, POS
	ld de, HelloWorldStr
	call StringPrint

	; set the bg palette
	ld a, NORMAL_PALETTE
	ld [rBGP], a

	; set the scroll position
	xor a  ; a = 0
	ld [rSCX], a
	ld [rSCY], a

	; turn the LCD on
	ld a, LCD_ON
	ld [rLCDC], a

	; disable sound
	ld [rNR52], a

	; enable only VBlank interrupts
	ld a, %00000001
	ld [rIE], a
	ei  ; actually enable them
