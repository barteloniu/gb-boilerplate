# Information about images

## Which colours to use

	| number in the pallet | colour in the image |
	| --------------------:| ------------------- |
	| 0                    | #ffffff             |
	| 1                    | #bbbbbb             |
	| 2                    | #444444             |
	| 3                    | #000000             |
