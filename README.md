# gb-boilerplate

## boilerplate code for a Game Boy project

## Acknowledgements

`hardware.inc` from [this repository](https://github.com/gbdev/hardware.inc).

The font is inspired by [Dina Programming Font](http://www.dcmembers.com/jibsen/download/61/) by Joergen Ibsen.

The images are licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

[RGBDS](https://github.com/rednex/rgbds) is the used assembler.

Also thanks to the people behind [BGB](http://bgb.bircd.org/) for making an awesome emulator.
